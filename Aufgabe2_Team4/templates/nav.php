<?php //Our responsive nav. Had to use a rel_path for all links, because of different file locations ?>
<nav>
	<ul class="topnav" id="myTopnav">
		<li>
			<div id="logo">
				<img class="mainlogo" src="<?php echo $template_vars['rel_path'];?>image/world_data_logo_v1.png"
					alt="world_data_logo" />
			</div>
		</li>
		<li class="liste"><a href="<?php echo $template_vars['rel_path'];?>#A1"><i class="fa fa-list-ul"
				aria-hidden="true">&nbsp;A1-Table</i></a></li>
		<li class="liste"><a href="<?php echo $template_vars['rel_path'];?>modules/parse.php"><i class="fa fa-list-ul"
				aria-hidden="true">&nbsp;A2-Parse</i></a></li>
		<li class="liste"><a href="<?php echo $template_vars['rel_path'];?>modules/save.php"><i class="fa fa-list-ul"
				aria-hidden="true">&nbsp;A2-Save</i></a></li>
		<li class="liste"><a href="<?php echo $template_vars['rel_path'];?>modules/print.php"><i class="fa fa-list-ul"
				aria-hidden="true">&nbsp;A2-Print</i></a></li>
		<li class="liste"><a href="<?php echo $template_vars['rel_path'];?>#A5"><i class="fa fa-list-ul"
				aria-hidden="true">&nbsp;A3-Rest</i></a></li>
		<li class="liste"><a href="<?php echo $template_vars['rel_path'];?>#A6"><i class="fa fa-list-ul"
				aria-hidden="true">&nbsp;A4-Vis</i></a></li>
		<li class="icon"><a href="javascript:void(0);"><i class="fa fa-bars"
				onclick="ResponsiveNav()"></i></a></li>
	</ul>
</nav>
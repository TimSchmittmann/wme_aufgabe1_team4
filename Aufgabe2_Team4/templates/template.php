<?php //Our main template. Everything is structured into separate files to allow easy modification. ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<?php include "head.php"; ?>
</head>
<body>
        <header>
        	<?php include "nav.php"; ?>
        </header>
    	<div class="page-container" id="page-container">
			<section class="explanation">
				<?php include "world_data_text.php"; ?>
        	</section>
			<?php include "show-hide.php" ?>
			<div class="table-container">
				<?php echo $template_vars['world_data_table']; ?>
			</div>
			<?php include 'show-hide.php'; ?>
			<?php include 'footer.php'; ?>
		</div>
		<script type="text/javascript">
			<?php echo $template_vars['end_of_doc_js']; ?>
		</script>
</body>
</html>
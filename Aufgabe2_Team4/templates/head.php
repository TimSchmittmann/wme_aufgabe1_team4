<?php //The head part with all meta_information and ressources to be loaded before rendering the page ?>
<title><?php echo $template_vars['title']; ?></title>
<meta charset="UTF-8">
<meta name="description" content="<?php echo $template_vars['meta_description']; ?>">
<meta name="author" content="Tim Schmittmann, Ervis Lilaj">
<meta name="keywords" content="HTML,CSS,XML,JavaScript,PHP">
<meta name="Team4" content="<?php echo $template_vars['meta_content']; ?>">
<link href="<?php echo $template_vars['rel_path'];?>css/reset.css" rel="stylesheet" type="text/css">
<link href="<?php echo $template_vars['rel_path'];?>css/font-awesome.css" rel="stylesheet" type="text/css" >
<link href="<?php echo $template_vars['rel_path'];?>css/index.css" rel="stylesheet" type="text/css" >
<link href="<?php echo $template_vars['rel_path'];?>css/content.css" rel="stylesheet" type="text/css">
<script src="<?php echo $template_vars['rel_path'];?>js/index.js" type="text/javascript" ></script>
<script src="<?php echo $template_vars['rel_path'];?>js/world-data-table.js" type="text/javascript" ></script>
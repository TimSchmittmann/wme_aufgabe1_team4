<?php ob_start(); ?>
<table id="world-data">
<thead>
	<tr>
		<th class="col1" data-col="1">ID</th>
		<th class="col2" data-col="2">Country<i class="fa fa-angle-up sort-icon sort" data-sort="asc" aria-hidden="true"></i><i class="fa fa-angle-down sort-icon sort" data-sort="desc" aria-hidden="true"></i></th>
		<th class="col3" data-col="3">birth rate / 1000</th>
		<th class="col4" data-col="4">cellphones / 100</th>
		<th class="col5" data-col="5">children / woman</th>
		<th class="col6" data-col="6">electric usage</th>
		<th class="col7" data-col="7">internet usage</th>
	</tr>
</thead>
<tbody>
</tbody>
</table>
<div id="drop-zone">
	<input type="file" id="file-upload" name="file" />
	<span class="drop-zone-text">Drop file here or click to upload</span>
</div>
<?php $template_vars['world_data_table'] = ob_get_clean(); ?>

<?php //Footer with copyright and some info about the exercise ?>
<footer>
	<div class="copyright">
		<div>Copyright &copy; world_data</div>
		<div><?php echo $template_vars['exercise']?> course excercise <strong>HTML, CSS, JS</strong> of the lecture Web and Multimedia Engineering</div>
	</div>
	<div class="created_by">
		<div>This solution has been created by:</div>
		<div>(s0954937) Ervis Lilaj und (s7740678) Tim Schmittmann - Team 4</div>
	</div>
</footer>
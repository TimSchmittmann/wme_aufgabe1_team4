<?php 
	require "world_data_parser.php";
	
	$dataParser = new WorldDataParser();
	$worldData = $dataParser->parseCSV("../data/world_data_v1.csv");

	//Print_r for human-readable output. Echo would only show "array"
	echo '<pre>'; print_r($worldData); echo '</pre>';

?>
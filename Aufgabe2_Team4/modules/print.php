<?php
    require "world_data_parser.php";
    
	$dataParser = new WorldDataParser();
	$worldData = $dataParser->parseCSV("../data/world_data_v1.csv");

    $saved = $dataParser->saveXML($worldData);
    
    $table = $dataParser->printXml("world_data.xml", "../xml_table.xsl");
    
    //Did some refactoring to prevent duplicate html code and to be able
    //to return only the table from $dataParser->saveXML and then integrate it into the html.
    //Now we use a global $template_vars array to configure our index template
    //Global variable, because it's the most concise and an appropriate way to handle this small task
    $template_vars = array();
    $template_vars['world_data_table'] = $table;
    
    $template_vars['title'] = "WME Aufgabe2_Team4";
    $template_vars['meta_description'] = "Aufgabe 2 WME. Nachbauen eines statischen HTML, CSS, JS Gerüsts";
    $template_vars['meta_content'] = "A2";
    
    $template_vars['exercise'] = "Second";
    //We got some problem with paths, because the print.php lies in another folder than index.php.
    //So now there is a rel_path variable
    $template_vars['rel_path'] = "../";
    //The js we need for this table is slightly less than for the csv one (no dropZone and parsing csv)
    $template_vars['end_of_doc_js'] = 'addSortHandler();';
    
    include "../templates/template.php";
?>
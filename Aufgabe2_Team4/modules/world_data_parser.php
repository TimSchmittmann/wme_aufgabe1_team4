<?php 

class WorldDataParser{
    
	/**
	 *
	 * Parses a csv file to prepare it for converting it to a xml file
	 *
	 * @param    string	$file	filepath that contains the data to parse
	 * @return   array	the parsed csv file as array. Each element is a row, 
	 * 					which itself contains columns as elements
	 *
	 */
    function parseCSV($file)    {
    	//Need read permission
        if (($cvsData = fopen($file, "r")) !== FALSE) {
            
            $arr = array(); 
            $hauptarray = array();
            $i = 0;
            $keys;
            //Parse the CSV
            while (($data = fgetcsv($cvsData, 0, ",")) !== FALSE) 
            {
                if($i == 0) 
                {
                	//First we save the keys, so we can use them as array indizes
                    $keys = $data;
                }
                else 
                {
                	//Use keys as array indizes to make parse output look better
                    for($j = 0;$j<sizeof($keys);$j++)
                        $arr[$keys[$j]] = $data[$j];
                    $hauptarray[] = $arr;            
                }
                
                $i = $i +1;
                }
            
            fclose($cvsData);
            
            return $hauptarray;
        }
        
        return false;

    }
    
    /**
     *
     * Saves array of data as xml file using the first array row as keys for the xml tags
     *
     * @param    array	$data	array with data where each element is a row that itself contains columns as elements 
     * @return   boolean	true when the xml was successfully written, false otherwise
     *
     */
   	function saveXML($data) {
		try {
			$doc  = new DomDocument();
			$doc->formatOutput   = true;
			
			// Add a root node to the document
			$root = $doc->createElement('countries');
			$root = $doc->appendChild($root);
	
			$keys = array_keys($data[0]);
            
			foreach($data as $row) {
				//Surround the data of each country with <country> tag
				$container = $doc->createElement('country');             
                                
				foreach($keys as $i => $key) {                    
					//Replace spaces by underscore, because it'll fail otherwise
					$child = $doc->createElement(str_replace(" ", "_", trim($key)));
					$child = $container->appendChild($child);
					//Remove trailing whitespace, because there is a lot
					$value = $doc->createTextNode(trim($row[$key]));
					$value = $child->appendChild($value);
				}
				$root->appendChild($container);
			}
			
			//Any step in file creation, file write or file close may fail,
			//give a warning and return false value. So we need to check the return values
			$strxml = $doc->saveXML();
			if($strxml) {
				$xmlData = fopen("world_data.xml", "w");

				if($xmlData && fwrite($xmlData, $strxml) && fclose($xmlData)) {
					return true;
				}
			}
			return false;
		} catch(exception $e) {
			//In case of any unexpected failure
			return false;			
		}
		
	}
    
	/**
	 *
	 * Convert xml data file with xsl stylesheet to valid html table filled by xml data
	 *
	 * @param    string	$xml Path to xml file
	 * @param    string	$xsl Path to xsl stylesheet
	 * @return   string valid html table filled by xml data or false on error
	 *
	 */
    function printXml($xml, $xsl){
    	//Very straightforward. Load XSL and XML and transform with XSLTProcessor
        $xslDoc = new DOMDocument();
        $xslDoc->load($xsl);

        $xmlDoc = new DOMDocument();
        $xmlDoc->load($xml);

        //It would be possible to initialize the XSLTProcessor in a constructor, 
        //but this makes the whole function a little more compact
        $proc = new XSLTProcessor();
        $proc->importStylesheet($xslDoc);
        return $proc->transformToXML($xmlDoc);
    }
    
}

?>
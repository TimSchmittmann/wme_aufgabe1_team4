<?php 
	require "world_data_parser.php";
	
	$dataParser = new WorldDataParser();
	$worldData = $dataParser->parseCSV("../data/world_data_v1.csv");

    $saved = $dataParser->saveXML($worldData);

    if($saved)
        echo "world_data.xml erfolgreich gespeichert!";
    else
        echo "Speichern fehlgeschlagen!";     
?>
<?php 

//For comment to template handling see modules/print.php
$template_vars = array();

$template_vars['title'] = "WME Aufgabe1_Team4";
$template_vars['meta_description'] = "Aufgabe 1 WME. Nachbauen eines statischen HTML, CSS, JS Gerüsts";
$template_vars['meta_content'] = "A1";

$template_vars['exercise'] = "First";

//Absolute path to csv file for table
$csv_path = "file://D:\\lvl3\\\git\\\informatik\\\WME\\\Aufgabe1_Team4\\\Aufgabe1_Team4\\\data\\\world_data_v1.csv";
$template_vars['rel_path'] = "";
$template_vars['end_of_doc_js'] = 'readTableDataFromCSV("'.$csv_path.'");';

//This is only the table skeleton with the dropZone. Table gets filled by javascript
include "templates/csv_table.php";
include "templates/template.php";
?>
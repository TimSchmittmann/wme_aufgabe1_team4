<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xml>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<xsl:comment>We only need a stylesheet for the table, because once we transform it to html we can pass it to our index template</xsl:comment>
	<table id="world-data">
		<xsl:comment>Our header is static. We need the header names for value selection anyway</xsl:comment>
	    <thead>
	        <tr>
	            <th class="col1">ID</th>
	            <th class="col2" data-col="2">Country
		            <i class="fa fa-angle-up sort-icon sort" data-sort="asc" aria-hidden="true"><xsl:comment>XSLT Engine has problems with self-closing tags... Force close</xsl:comment></i>
		            <i class="fa fa-angle-down sort-icon sort" data-sort="desc" aria-hidden="true"><xsl:comment>XSLT Engine has problems with self-closing tags... Force close</xsl:comment></i>
	            </th>
				<th class="col3">birth rate / 1000</th>
	            <th class="col4">cellphones / 100</th>
	            <th class="col5">children / woman</th>
	            <th class="col6">electric usage</th>
	            <th class="col7">internet usage</th>
	        </tr>
	        </thead>
		    <xsl:comment>Loop through all country elements in our XML File and create a row for each</xsl:comment>
	        <tbody>
	         <xsl:for-each select="countries/country">
	            <tr>
	                <td class="col1" data-col="1"> <xsl:value-of select="id"/></td>
	                <td class="col2" data-col="2"> <xsl:value-of select="name"/></td>
	                <td class="col3" data-col="3"> <xsl:value-of select="birth_rate_per_1000"/></td>
	                <td class="col4" data-col="4"> <xsl:value-of select="cell_phones_per_100"/></td>
	                <td class="col5" data-col="5"> <xsl:value-of select="children_per_woman"/></td>
	                <td class="col6" data-col="6"> <xsl:value-of select="electricity_consumption_per_capita"/></td>
	                <td class="col7" data-col="7"> <xsl:value-of select="internet_user_per_100"/></td>
	            </tr>
	          </xsl:for-each>
	    	</tbody>
	</table>
</xsl:template>
</xsl:stylesheet>
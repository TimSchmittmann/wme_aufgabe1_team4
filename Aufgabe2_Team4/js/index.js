
function ResponsiveNav() {
    var x = document.getElementById("myTopnav");
    var y = document.getElementById("page-container");
    if (x.className === "topnav") {
        x.className += " responsive";
        y.className += " responsive-container"
    } else {
        x.className = "topnav";
        y.className = "page-container"
    }
}

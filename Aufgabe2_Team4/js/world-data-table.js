function showHideCol(col) {
	var deviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
	//Disable links for higher cols on devices with lower width 
	if((deviceWidth >= 680 && col < 7) || col < 5 || deviceWidth > 950) {
		var colFields = document.getElementsByClassName("col"+col);
		for(let i = 0; i < colFields.length; i++) {
			if(colFields[i].style.display !== "none") {
				colFields[i].style.display="none";
			} else {
				colFields[i].style.display="table-cell";
			}
		}
	}
}

//https://www.quora.com/What-is-the-best-way-to-read-a-CSV-file-using-JavaScript-not-JQuery
function readTextFile(file, callback)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if((rawFile.status === 200 || rawFile.status == 0) && rawFile.responseText !== "")
            {
                callback(rawFile.responseText);
                hideDropZone();
            } else {
            	initDropZone();
            }
        }
    }
    try {
    	rawFile.send(null);
    } catch(e) {
    	initDropZone();
    }
}

function hideDropZone() {
	document.getElementById("drop-zone").style.display="none";
}

//File drop handling from: https://www.html5rocks.com/en/tutorials/file/dndfiles/
function initDropZone() {
	// Check for the various File API support.
	if (window.File && window.FileReader) {
		// Setup the dnd listeners.
		document.getElementById("file-upload").addEventListener("change", handleFileSelect, false);

		var dropZone = document.getElementById("drop-zone");
		dropZone.addEventListener("dragover", handleDragOver, false);
		dropZone.addEventListener("drop", handleFileSelect, false);
		dropZone.addEventListener("click", handleDropZoneClick, false);
		// Great success! All the File APIs are supported.
	} else {
		alert("File APIs are not fully supported in this browser and local files are not enabled. "+
		"Please use a modern browser or enable local file access.");
	}
}

function handleDropZoneClick() {
	document.getElementById("file-upload").click();
}

function handleFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();

    var files;
    if(evt.type === "change") {
    	files = evt.target.files; // FileList object.
    } else {
    	files = evt.dataTransfer.files; // FileList object.
    }

    // files is a FileList of File objects. Only allow one file.
    f = files[0];
	var reader = new FileReader();

    reader.onload = (function(theFile) {
      return function(e) {
    	  processData(e.target.result);
      };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsText(f);
}

function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = "copy"; // Explicitly show this is a copy.
}

//https://www.quora.com/What-is-the-best-way-to-read-a-CSV-file-using-JavaScript-not-JQuery
function processData(allText) {

	var allTextLines = allText.split(/\r\n|\n/);
	var headers = allTextLines[0].split(",");
	var lines = [];

	for (var i=1; i<allTextLines.length; i++) {
		var data = allTextLines[i].split(",");
		if (data.length == headers.length) {
			var tarr = [];
			for (var j=0; j<headers.length; j++) {
				tarr.push(data[j]);
			}
			lines.push(tarr);
		}
	}
	
	insertDataIntoTable(lines);
}

var colMapping = {
		1:0,
		2:1,
		3:2,
		4:3,
		5:4,
		6:5,
		7:9
}

function insertDataIntoTable(lines) {
	var tableBody = document.getElementById("world-data").getElementsByTagName("tbody")[0];
	var cols = document.querySelectorAll("#world-data > thead > tr > th[class*=col]");
	for(let i=lines.length-1; i>=0; i--) {
		var row = tableBody.insertRow(0);
		for(let j=0; j<cols.length; j++) {
			var cell=row.insertCell(j);
			var colNr = cols[j].dataset.col;
			cell.className = "col"+colNr;
			cell.dataset.col = colNr;
			cell.innerHTML = lines[i][colMapping[colNr]];
		}
	}
	
	
	//After data is inserted add sort listener/handler and hide dropZone
	hideDropZone();
	addSortHandler();
//	dropZone.addEventListener("click", handleDropZoneClick, false);
	
}

function addSortHandler() {
	var sortElements = document.getElementsByClassName("sort");
	for(let i=0; i<sortElements.length; i++) {
		sortElements[i].addEventListener("click", sortClickHandler, false);
	}
}

function sortClickHandler(e) {
	e = e || window.event;
	var target = e.target || e.srcElement;
	if(typeof target.parentNode.dataset.col !== "undefined" 
		&& typeof target.dataset.sort !== "undefined") {
		var colToSort = target.parentNode.dataset.col;
		var sortType = target.dataset.sort;
		sortRowsByCol(colToSort, sortType);
	}
}

function sortRowsByCol(col, sortType) {
	var tbody =  document.querySelectorAll("#world-data > tbody")[0];
	var rows = tbody.children;
	var arrRows = [].slice.call(rows);
//	document.querySelectorAll("#world-data > tbody > tr");
	arrRows.sort(compareOnCol(col, sortType));
	while (tbody.firstChild) {
	    tbody.removeChild(tbody.firstChild);
	}
	for(let i=0; i<arrRows.length; i++) {
		tbody.appendChild(arrRows[i]);
	}
}

function compareOnCol(col, sortType) {
    return function(a, b) {
        for(let i=0; i < a.childNodes.length; i++) {
        	if(a.childNodes[i].dataset.col === col && b.childNodes[i].dataset.col === col) {
        		var cmp = a.childNodes[i].innerHTML.toLocaleLowerCase().localeCompare(b.childNodes[i].innerHTML.toLocaleLowerCase());
        		return (sortType === "desc") ? -cmp : cmp;
        	}
        }
        return 0;
    };
}

function readTableDataFromCSV(file) {
	readTextFile(file, processData);
	
}
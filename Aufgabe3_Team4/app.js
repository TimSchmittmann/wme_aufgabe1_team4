// DO NOT CHANGE!
//init app with express, util, body-parser, csv2json
var express = require('express');
var app = express();
var sys = require('util');
var path = require('path');
var bodyParser = require('body-parser');
var Converter = require("csvtojson").Converter;

//register body-parser to handle json from res / req
app.use( bodyParser.json() );

//register public dir to serve static files (html, css, js)
app.use( express.static( path.join(__dirname, "public") ) );

// END DO NOT CHANGE!
app.use(bodyParser.urlencoded({extended: true}));

/**************************************************************************
****************************** csv2json *********************************
**************************************************************************/
var csvConverter=new Converter({});
var json;

csvConverter.fromFile("world_data.csv",function(err,result){
	 json = result;
});
/**************************************************************************
********************** handle HTTP METHODS ***********************
**************************************************************************/


app.get('/items', function(req, res){
	res.statusCode = 200;
	res.setHeader('Content-Type', 'application/json');
	res.end(JSON.stringify(json));
});

app.get('/items/:id', function(req, res){
	
	var id = Number(req.params.id);
	
	var result = false;
	
	json.forEach(function(country) {
		if(Number(country['id']) === id) {
			result = country;
		}
	});
	if(result) {
		res.status(200);
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify([result]));
	} else {
		res.status(400);
		res.setHeader('Content-Type', 'text/plain');
		res.end("No such id "+id+" in database.");
	}
	
});

app.get('/items/:id1/:id2', function(req, res){
	var result = [];
	var id1 = Number(req.params.id1);
	var id2 = Number(req.params.id2);
	
	json.forEach(function(country) {
		if(Number(country['id']) >= id1 && Number(country['id']) <= id2) {
			result.push(country);
		}
	});
	if(result.length > 0) {
		res.status(200);
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(result));
	} else {
		res.status(400);
		res.setHeader('Content-Type', 'text/plain');
		res.end("Range not possible.");
	}
});

app.get('/properties', function(req, res){
	if(json.length > 0) {
		res.status(200);
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(Object.keys(json[0])));
	} else {
		res.status(400);
		res.setHeader('Content-Type', 'text/plain');
		res.end("No entries in database");
	}
});

app.get('/properties/:num', function(req, res){
	if(typeof Object.keys(json[0])[req.params.num] !== "undefined") {
		res.status(200);
		res.setHeader('Content-Type', 'application/json');
		res.end(Object.keys(json[0])[req.params.num]);
	} else {
		res.status(400);
		res.setHeader('Content-Type', 'text/plain');
		res.end("No such property available");
	}
});

app.post('/items', function(req, res){
	var item = req.body;
    var last_key = Object.keys(json)[Object.keys(json).length - 1];
    var last = json[last_key];

    var item = ({ 'id' :  +last.id + 1,
                'name': req.body.name,
                'birth': req.body.birth,
                'cellphone': req.body.cellphone
            });
	json.push(item);
	res.status(200);
	res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
	res.statusMessage = "Added country "+item.name+" to list!";
	res.end(JSON.stringify([item]));
});


app.delete('/items', function(req, res){
	if(json.length > 0) {
		res.status(200);
		res.setHeader('Content-Type', 'text/plain');
		res.end("Deleted last country: "+json.pop()['name']);
	} else {
		res.status(400);
		res.setHeader('Content-Type', 'text/plain');
		res.end("No more entries");
	}
});

app.delete('/items/:id', function(req, res){
	var idExists = false;
	var id = Number(req.params.id);
	for(let i = json.length - 1; i >= 0; i--) {
	    if(Number(json[i]['id']) == id) {
	    	idExists=true;
	        json.splice(i, 1);
	    }
	}
	
	if(idExists) {
		res.status(200);
		res.setHeader('Content-Type', 'text/plain');
		res.end("Item "+id+" deleted successfully.");
	} else {
		res.status(400);
		res.setHeader('Content-Type', 'text/plain');
		res.end("No such id "+id+" in database.");
	}
});

app.get('/propitems/:name', function(req, res){
	var result = [];
	
	if(typeof req.params.name !== "undefined") {
		json.forEach(function(country) {
			if(typeof country[req.params.name] !== "undefined"){
				result.push({id: country['id'], prop: country[req.params.name]});
			} 
		});
	}
	if(result.length > 0) {
		res.status(200);
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(result));
	} else {
		res.status(400);
		res.setHeader('Content-Type', 'text/plain');
		res.end("No values for property.");
	}
});


// DO NOT CHANGE!
// bind server to port
var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});
$(document).ready(function() {
	$("#country_filter").submit(function(e) {
		e.preventDefault();
		var id = $("#country_filter_id").val();
		var range = $("#country_filter_range").val();
         var ids = range.split("-");
        
		var url = ids.length > 1 ? "http://localhost:3000/items/"+ids[0]+"/"+ids[1] 
				: "http://localhost:3000/items/"+id;
		
		$.ajax({
	        url: url,
	        type: "GET",
	        async: true,
	        success: function (response) {
	        	insertTable(response);
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	           console.log(textStatus, errorThrown);
	        }
	    });

	});
	

    $("#country_add").submit(function(e) {
        e.preventDefault();

        $.ajax({
            url: "http://localhost:3000/items",
            type: "POST",
            data: JSON.stringify({
                'name': $("#country_name").val(),
                'birth': $("#country_birth").val(),
                'cellphone': $("#country_cellphone").val()
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function() {
                console.log("country_add");
            }
        });      
        deleteRows();
        getItems()
    });
    
    $("#show_selected_prop").click(function(e) {
    	e.preventDefault();
    	showPropertyItems($("#prop_selection").children(":selected").val());
    });
    
    $("#hide_selected_prop").click(function(e) {
    	e.preventDefault();
    	hidePropertyItems($("#prop_selection").children(":selected").val());
    });

	
	$("#country_delete").submit(function(e) {
		e.preventDefault();
		var id = $("#country_delete_id").val();
		
		var url = typeof id !== 'undefined' ? "http://localhost:3000/items/"+id 
				: "http://localhost:3000/items";
		
		$.ajax({
	        url: url,
	        type: "DELETE",
	        async: true,
	        success: function (response) {
	        	console.log(response);
	        	getItems();
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	           console.log(textStatus, errorThrown);
	        }
	    });
		
	});

	getItems();
	getProperties();

	
});

function showPropertyItems(property) {
	$.ajax({
        url: "/propitems/"+property,
        type: "GET",
        async: true,
        success: function(data) {
            addToTable(property, data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function hidePropertyItems(property) {
	var exist = 0;
	for(let i=1; i<$("#table_head").children().length; i++) {
		if($($("#table_head").children()[i]).html() == property) {
			exist = i;
		}
	}
	if(exist !== 0) {
		$("#table_head").children(":nth-child("+(exist+1)+")").remove();
		$("#table_body").children().each(function(index, row) {
			$(row).children(":nth-child("+(exist+1)+")").remove();
		});
	} 
}

function addToTable(property, data) {
	var exist = 0;
	for(let i=1; i<$("#table_head").children().length; i++) {
		if($($("#table_head").children()[i]).html() == property) {
			exist = i;
		}
	}
	if(exist === 0) {
		var th = $("<th>");
		th.html(property);
		$("#table_head").append(th);
		$("#table_body").children().each(function(index, row) {
			var id = $(row).children(":first-child").html();
			var td = $("<td>");
			td.html(data[id-1].prop);
			$(row).append(td);
		});
	} else {
		$("#table_body").children().each(function(index, row) {
			var id = $(row).children(":first-child").html();
			$(row).children(":nth-child("+exist+1+")").html(data[id-1].prop);
		});
	}
}

function getItems() {
	$.ajax({
        url: "/items",
        type: "GET",
        async: true,
        success: function(data) {
            insertTable(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function getProperties() {
	$.ajax({
        url: "/properties",
        type: "GET",
        async: true,
        success: function(data) {
            insertProperties(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function deleteRows() {

    $("#table_body").find("tr").remove();
}

function insertProperties(data) {
	$.each(data, function(key, val) {
		var option = $("<option>");
		option.val(val);
		option.text(val)
		$("#prop_selection").append(option);
	});
} 

function insertTable(data) {
  
	$("#table_body").empty();
	for(let i=7; i<$("#table_head").children().length; i++) {
		$("#table_head").children()[i].remove();
	}
	$.each(data, function(key, dataRow) {
		$("#table_body").append($("<tr>"));
		var row = $("#table_body tr").last();
		$.each(dataRow, function(key, col) {
			if($("#table_head").children().length > row.children().length) {
				row.append($("<td>"));
				$("#table_body tr td").last().html(col);
			}
		});
	});
    
    
}